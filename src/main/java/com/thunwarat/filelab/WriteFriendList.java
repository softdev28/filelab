/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class WriteFriendList {

    public static void main(String[] arge) {
        ArrayList<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend("Worawit", 43, "0881234567"));
        friendList.add(new Friend("Jakaman", 45, "0831311024"));
        friendList.add(new Friend("Moowhan", 45, "0861489152"));
        
        FileOutputStream fos = null;
        try {
            //Friend friend1 = new Friend("Worawit", 43, "0881234567");
            //Friend friend2 = new Friend("Jakaman", 45, "0831311024");
            //Friend friend3 = new Friend("Moowhan", 45, "0861489152");
            //friend.dat
            File file = new File("Listfriend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
